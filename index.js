const fs = require("fs");
const Parser = require("rss-parser");
const moment = require("moment");

const CHANGELOG_URL = `https://about.gitlab.com/handbook/changelog.rss`;
const OUT_DIR = "./public";
const OUT_FILENAME = "index.html";

let parser = new Parser({
  customFields: {
    item: ["content"]
  }
});

console.log(`Loading from ${CHANGELOG_URL}...`);
parser.parseURL(CHANGELOG_URL).then(res => {
  const currentTime = moment().format();
  console.log(`Loading from ${CHANGELOG_URL}... Done!`);
  console.log(`Loaded at ${currentTime}`);

  let contentItems = '';
  let tocItems = '';
  res.items.forEach((item) => {

    // Replace links and get sections
    item.section = ''
    item.content = item.content.replace(
      /source\/handbook\/(.*?)\/index\.html.*/,
      (match, page) => {
        // Get first section
        item.section = item.section || page.split('/')[0]
        return `<a  title='See Handbook Page' href='https://about.gitlab.com/handbook/${page}/' target='_blank'>${match}</a>`;
      }
    );

    // Get shorter id
    item.id = item.id.replace(
      'https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/',
      ''
    );

    // Fill table of contents
    tocItems +=
    `<li class='item'>` +
      `<a href='#mr-${item.id}'>` +
        `${item.section ? '<strong>' + item.section + '</strong>' : ''}` +
        `${item.title}` +
      `</a>` +
    `</li>`

    // Fill content
    const itemTime = moment(item.isoDate).fromNow();
    contentItems +=
      `<div id='mr-${item.id}' class='item'>` +
      `<h2>${item.title}</h2>` +
      `<p class='author'>${item.author} ` +
        `<a href='${item.link}' target='_blank' title='See Merge Request'>(${itemTime})</a>` +
      `</p>` +
      `<pre>${item.content}</pre>` +
      `</div>`;
  });

  const html = `
  <html>
    <head>
      <title>Gitlab Handbook Latest Updates | Updated at ${currentTime}</title>
      <link rel="stylesheet" href="./styles.css" />
    </head>
    <body>
      <a class='fork-me' target="_blank" href="https://gitlab.com/miguelrincon/gl-handbook-news"><img src='./ribbon.png'/></a>
      <div class='sidebar'>
        <ul>${tocItems}</ul>
      </div>
      <div class='content'>
        <header>
          <h1>Gitlab Handbook Latest Updates</h1>
          <p>Updated at <strong>${currentTime}</strong>, oldest update from ${moment(res.items[res.items.length-1].isoDate).fromNow()}</p>
        </header>
        ${contentItems}
      </div>
    </body>
  </html>
  `;

  console.log(`Saving file...`);
  if (!fs.existsSync(OUT_DIR)) {
    fs.mkdirSync(OUT_DIR);
  }
  fs.writeFileSync(OUT_DIR + "/" + OUT_FILENAME, html);
  console.log(`Saving file... Done!`);

  console.log(`Handbook changelog saved! ${res.items.length} entries loaded.`);
});
