## GitLab Handbook Changes Page

**Stay up to date to the latest changes in the GitLab's Handbook:**

Bookmark it:

[https://miguelrincon.gitlab.io/gl-handbook-news/](https://miguelrincon.gitlab.io/gl-handbook-news/)

### Motivation

GitLab's handbook is a living document to which **everyone can contribute**. The handbook changes very often (a few times per day), and  as a member of the GitLab community I wanted to stay up to date on the latest changes in the company.

I tried to use an RSS feed reader, but I was not happy with it to keep track of the changes.

### GitLab Tooling

I used Gitlab tools, like the **Pipeline Schedules** and **Pages**, I put together a list of the latest changes in handbook.

### Install and use

```sh
$ npm install
$ npm start
```

## Next iteration

Currently this project uses the RSS feed of the handbook. I found this has the following limitations:

- The feed is only updated a few times per day.
- The amount of entries is limited to about 3-4 days ago.

The next iteration could be done using Gitlab's API to fetch last merges to master.
